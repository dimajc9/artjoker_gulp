let departmentNumber: Record<string, number> = {
    hall: 1,
    kitchen: 2,
    warehouse: 3,
}

let positionNumber: Record<string, number> = {
    Head: 1,
    bartender: 2,
    chef: 3,
    loader: 4,
    sorter: 5,
    waiter: 6,
    сook: 7,
}

interface TypeEmployee{
    name: string,
    surname: string,
    position: string,
    positionNumber: number,
    salary: number,
    works: boolean,
    departmentNumber: number,
    departmentName: string,
}

let employeeObjectss: TypeEmployee[] = [
    {
        name: 'Alex',
        surname: 'Alexis',
        position: 'waiter',
        positionNumber: 6,
        salary: 1200,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Aleksandr',
        surname: 'Hoggarth',
        position: 'bartender',
        positionNumber: 2,
        salary: 1500,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Kirill',
        surname: 'Nash',
        position: 'bartender',
        positionNumber: 2,
        salary: 1600,
        works: false,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Aleksey',
        surname: 'Holiday',
        position: 'waiter',
        positionNumber: 6,
        salary: 1100,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    },
    {
        name: 'Anatoly',
        surname: 'James',
        position: 'Head',
        positionNumber: 1,
        salary: 2100,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Aleksey',
        surname: 'Keat',
        position: 'waiter',
        positionNumber: 6,
        salary: 1100,
        works: false,
        departmentNumber: 1,
        departmentName: 'hall',
    },
    {
        name: 'Artur',
        surname: 'Kendal',
        position: 'Head',
        positionNumber: 1,
        salary: 2400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    },
    {
        name: 'Artem',
        surname: 'Kelly',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Boris',
        surname: 'Kennedy',
        position: 'сook',
        positionNumber: 7,
        salary: 1450,
        works: false,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Vadim',
        surname: 'Kennett',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Valentin',
        surname: 'Kingsman',
        position: 'сook',
        positionNumber: 7,
        salary: 1300,
        works: false,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Valeriy',
        surname: 'Kirk',
        position: 'chef',
        positionNumber: 3,
        salary: 1800,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Mikhail',
        surname: 'Nevill',
        position: 'chef',
        positionNumber: 3,
        salary: 2100,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Vasily',
        surname: 'Laird',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Viktor',
        surname: 'Lamberts',
        position: 'sorter',
        positionNumber: 5,
        salary: 1000,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Vitaly',
        surname: 'Larkins',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Vladimir',
        surname: 'Lawman',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Gleb',
        surname: 'Leman',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: false,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Grigory',
        surname: 'Macey',
        position: 'sorter',
        positionNumber: 5,
        salary: 1000,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Daniil',
        surname: 'Mason',
        position: 'loader',
        positionNumber: 4,
        salary: 950,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Maksim',
        surname: 'Oldman',
        position: 'loader',
        positionNumber: 4,
        salary: 1050,
        works: false,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
];

class Restaurant {
    employeeObjects: TypeEmployee[];
    numbDepartament: number[]; 

    constructor(param: TypeEmployee[]){
        this.numbDepartament = [];
        this.employeeObjects = param;
    }

    sumSalariesDepartment(){
        let sum: Record<string, number> = {};

        this.employeeObjects.map((item) => {
            sum[item.departmentName] = sum[item.departmentName] || 0;
            sum[item.departmentName] += item.salary;
        });

        return sum;
    }

    averageSalaryDepartment(){
        let meanSalary: Record<string, number[]> = {};
        let result: Record<string, number>= {};

        this.employeeObjects.map((item) => {
            meanSalary[item.departmentName] = meanSalary[item.departmentName] || [];
        
            for(let index in meanSalary){
                if(index === item.departmentName){
                    meanSalary[index].push(item.salary);
                }
            }
        });

        function average(name: string, items: number[]): void {
            let sum: number = 0;

            for (let i: number = 0; i < items.length; i++) {
                sum += items[i];
            }
            
            result[name] = Math.floor(sum / items.length);
        }
        
        for(let index in meanSalary){
            average(index, meanSalary[index]);
        }

        return result;
    }

    salarysDepartment(callback: (min: number, max: number) => number){
        let departmentName: Record<string, number[]> = {};
        let result: Record<string, number>= {}

        this.employeeObjects.map((item) => {
            departmentName[item.departmentName] = departmentName[item.departmentName] || [];
            for(let index in departmentName){
                if(index === item.departmentName){
                    departmentName[index].push(item.salary);
                }
            }
        });

        for(let index in departmentName){
            departmentName[index].sort(callback);
            result[index] = departmentName[index][0];
        }

        return result;
    }
        
    salarysPosition(colback: (min: number, max: number) => number){
        let position: Record<string, number[]> = {};
        let result: Record<string, number>= {}

        this.employeeObjects.map((item) => {
            position[item.position] = position[item.position] || [];
            for(let index in position){
                if(index === item.position){
                    position[index].push(item.salary);
                }
            }
        });

        for(let index in position){
            position[index].sort(colback);
            result[index] = position[index][0];
        }

        return result;
    }
        
    countDismissed(){
        let result: Record<string, number> = {};

        this.employeeObjects.map((item) => {
            result[item.departmentName] = result[item.departmentName] || 0;
            result.length = result.length || 0;

            if(item.works === false){
                ++result[item.departmentName];
                ++result.length;
            }
        });

        return result;
    }
        
    departmentsNoManager(){
        let mass: TypeEmployee[] = [];
        let result: Array<string> = [];

        this.employeeObjects.map((item) => {
            if(item.positionNumber === 1 && item.works){
                this.numbDepartament.push(item.departmentNumber);
            }

            mass.push(item);
        });

        for(let index of this.numbDepartament){
            for(let i: number = 0; i < mass.length; i++){
                if(mass[i].departmentNumber === index){
                    mass.splice(i--, 1);
                }
            }
        }

        for(let index in mass){
            result.push(mass[index].departmentName);
        }

        let prims = {}
        
        return result.filter(function(item) {
            return prims.hasOwnProperty(item) ? false : (prims[item] = true);
        });
    }
}

let restaurant = new Restaurant(employeeObjectss);

class Statistics{
    sumSalariesDepartmentBlock: HTMLElement | null;
    averageSalaryDepartmentBlock: HTMLElement | null;
    salarysDepartmentBlock: HTMLElement | null;
    salarysPositionBlock: HTMLElement | null;
    countDismissedBlock: HTMLElement | null;
    departmentsNoManagerBlock: HTMLElement | null;
    constructor(){
        this.sumSalariesDepartmentBlock = document.querySelector<HTMLElement>('.sumSalariesDepartmentResult') as HTMLElement;
        this.averageSalaryDepartmentBlock = document.querySelector<HTMLElement>('.averageSalaryDepartmentResult') as HTMLElement;
        this.salarysDepartmentBlock = document.querySelector<HTMLElement>('.SalarysDepartmentResult') as HTMLElement;
        this.salarysPositionBlock = document.querySelector<HTMLElement>('.SalarysPositionResult') as HTMLElement;
        this.countDismissedBlock = document.querySelector<HTMLElement>('.countDismissedResult') as HTMLElement;
        this.departmentsNoManagerBlock = document.querySelector<HTMLElement>('.departmentsNoManagerResult') as HTMLElement;
    }

    sumSalariesDepartment(){
        let data = restaurant.sumSalariesDepartment();
        this.sumSalariesDepartmentBlock!.innerHTML = '';
        const sumSalariesDepartmentResult: HTMLDivElement = document.createElement('div');
        sumSalariesDepartmentResult.className = 'sumSalariesDepartmentResult';
        for(let index in data){
            sumSalariesDepartmentResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        return this.sumSalariesDepartmentBlock!.append(sumSalariesDepartmentResult);
    }
    
    averageSalaryDepartment(){
        let data: Record<string, number> = restaurant.averageSalaryDepartment();
        this.averageSalaryDepartmentBlock!.innerHTML = '';
        const averageSalaryDepartmentResult: HTMLDivElement = document.createElement('div');
        averageSalaryDepartmentResult.className = 'averageSalaryDepartmentResult';

        for(let index in data){
            averageSalaryDepartmentResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        return this.averageSalaryDepartmentBlock!.append(averageSalaryDepartmentResult);
    }

    salarysDepartment(data?: Record<string, number>){
        data = data || restaurant.salarysDepartment((min: number, max: number) => min - max);

        this.salarysDepartmentBlock!.innerHTML = '';
        const salarysDepartmentResult: HTMLDivElement = document.createElement('div');
        salarysDepartmentResult.className = 'salarysDepartmentResult';

        for(let index in data){
            salarysDepartmentResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        const buttonMin: HTMLElement = document.createElement('button');
        buttonMin.innerText = 'min';
        buttonMin.addEventListener('click', this.salarysDepartmentOnCallback.bind( this, salarysDepartmentResult, ((min: number, max: number) => min - max) ));
        
        const buttonMax: HTMLElement = document.createElement('button');
        buttonMax.innerText = 'max';
        buttonMax.addEventListener('click', this.salarysDepartmentOnCallback.bind( this, salarysDepartmentResult, ((min: number, max: number) => max - min) ));

        salarysDepartmentResult.append(buttonMin, buttonMax);

        return this.salarysDepartmentBlock!.append(salarysDepartmentResult);
    }

    salarysPosition(data?: Record<string, number>){
        data = data || restaurant.salarysPosition((min: number, max: number) => min - max);

        this.salarysPositionBlock!.innerHTML = '';
        const salarysPositionResult: HTMLDivElement = document.createElement('div');
        salarysPositionResult.className = 'salarysPositionResult';

        for(let index in data){
            salarysPositionResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        const buttonMin: HTMLElement = document.createElement('button');
        buttonMin.innerText = 'min';
        buttonMin.addEventListener('click', this.salarysPositionOnCallback.bind( this, salarysPositionResult, ((min: number, max: number) => min - max)));

        const buttonMax: HTMLElement = document.createElement('button');
        buttonMax.innerText = 'max';
        buttonMax.addEventListener('click', this.salarysPositionOnCallback.bind(this, salarysPositionResult, ((min: number, max: number) => max - min)));

        salarysPositionResult.append(buttonMin, buttonMax);
        return this.salarysPositionBlock!.append(salarysPositionResult);
    }

    countDismissed(){
        let data: Record<string, number> = restaurant.countDismissed();
        this.countDismissedBlock!.innerHTML = '';
        const countDismissedResult: HTMLDivElement = document.createElement('div');
        countDismissedResult.className = 'countDismissedResult';

        for(let index in data){
            countDismissedResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        return this.countDismissedBlock!.append(countDismissedResult);
    }

    departmentsNoManager(){
        let data: any = restaurant.departmentsNoManager();
        this.departmentsNoManagerBlock!.innerHTML = '';
        const departmentsNoManagerResult: HTMLDivElement = document.createElement('div');
        departmentsNoManagerResult.className = 'departmentsNoManagerResult';
        for(let index in data){
            departmentsNoManagerResult.innerHTML += `
                <p class="infoText">
                    ${data[index]}
                </p>
            `;
        }
        
        return this.departmentsNoManagerBlock!.append(departmentsNoManagerResult);
    }

    salarysPositionOnCallback(param: HTMLElement, callback: (min: number, max: number) => number){
        param.innerText = '';
        this.salarysPosition(restaurant.salarysPosition(callback));
    }

    salarysDepartmentOnCallback(param: HTMLElement, callback: (min: number, max: number) => number){
        param.innerText = '';
        this.salarysDepartment(restaurant.salarysDepartment(callback));
    }
}

const statistics = new Statistics();

class RenderCards{
    container: HTMLElement | null;
    modal: HTMLElement | null;
    trueAdnFalse: boolean | null;
    eventActive: Record<string, boolean>;

    constructor(){
        this.container = document.querySelector<HTMLElement>('.container') as HTMLElement;
        this.modal = document.querySelector<HTMLElement>('.modal') as HTMLElement;
        this.trueAdnFalse = null;
        this.eventActive = {
            "true": true,
            "false": false
        }
        this.renderUsers();
    }
    
    createCard(item: TypeEmployee, index: number){
        const buttonChange: HTMLElement = document.createElement('button');
        const buttonDelete: HTMLElement = document.createElement('button');

        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));
        buttonChange.addEventListener('click', this.chengeUsers.bind(this, item, index));
        
        const card: HTMLDivElement = document.createElement('div');
        card.className = 'users';
        const usersData: HTMLDivElement = document.createElement('div');
        usersData.className = 'users_data';
    
        const name: HTMLElement = document.createElement('P');
        name.innerText = `name: ${item.name}`;
        const surname: HTMLElement = document.createElement('P');
        surname.innerText = `surname: ${item.surname}`;
        const position: HTMLElement = document.createElement('P');
        position.innerText = `position: ${item.position}`;
        const salary: HTMLElement = document.createElement('P');
        salary.innerText = `salary: ${item.salary}`;
        const works: HTMLElement = document.createElement('P');
        works.innerText = `works: ${item.works}`;
        const departmentName: HTMLElement = document.createElement('P');
        departmentName.innerText = `departmentName: ${item.departmentName}`;
    
        const usersButtons: HTMLDivElement = document.createElement('div');
        usersButtons.className = 'users_buttons';
        buttonChange.innerText = 'Change';
        buttonDelete.innerText = 'Delete';
        usersButtons.append(buttonChange, buttonDelete);
    
        usersData.append(name, surname, position, salary, works, departmentName);
        card.append(usersData, usersButtons);
        
        return card;
    }

    renderUsers(){
        this.container!.innerHTML = '';
        const cards: HTMLElement[] = employeeObjectss.map((item, index) => this.createCard(item, index));
        this.container!.append(...cards);
    }

    createModalUsers(item?: TypeEmployee , index?: number){
        this.modal!.innerHTML = '';
        
        const modalChange: HTMLElement = document.createElement('form');
        modalChange.className = 'modalChange';

        const modalChangeCap: HTMLElement = document.createElement('P');
        modalChangeCap.className = 'modalChange_cap';
        modalChangeCap.innerText = 'User';

        const inputName: HTMLInputElement = document.createElement('input');
        inputName.name = 'name';
        if(this.trueAdnFalse) {
            inputName.placeholder = item!.name;
        }
        const name: HTMLElement = document.createElement('span');
        name.innerText = 'name:';
        name.append(inputName);

        const inputSurname: HTMLInputElement = document.createElement('input');
        if(this.trueAdnFalse) {
            inputSurname.placeholder = item!.surname;
        }
        inputSurname.name = 'surname';

        const surname: HTMLElement = document.createElement('span');
        surname.innerText = 'Surname:';
        surname.append(inputSurname);

        const selectPosition: HTMLSelectElement = document.createElement('select');
        selectPosition.name = 'position';

        const worksOption: HTMLOptionElement = document.createElement('option');
        if(this.trueAdnFalse) {
            worksOption.innerText = item!.position;
        }
        const worksOptionHead: HTMLOptionElement = document.createElement('option');
        worksOptionHead.innerText = 'Head';
        worksOptionHead.value = 'Head';

        const worksOptionBartender: HTMLOptionElement = document.createElement('option');
        worksOptionBartender.innerText = 'bartender';
        worksOptionBartender.value = 'bartender';

        const worksOptiOnchef: HTMLOptionElement = document.createElement('option');
        worksOptiOnchef.innerText = 'chef';
        worksOptiOnchef.value = 'chef';

        const worksOptionLoader: HTMLOptionElement = document.createElement('option');
        worksOptionLoader.innerText = 'loader';
        worksOptionLoader.value = 'loader';

        const worksOptionSorter: HTMLOptionElement = document.createElement('option');
        worksOptionSorter.innerText = 'sorter';
        worksOptionSorter.value = 'sorter';

        const worksOptionWaiter: HTMLOptionElement = document.createElement('option');
        worksOptionWaiter.innerText = 'waiter';
        worksOptionWaiter.value = 'waiter';

        const worksOptionCook: HTMLOptionElement = document.createElement('option');
        worksOptionCook.innerText = 'сook';
        worksOptionCook.value = 'сook';

        const position: HTMLSpanElement = document.createElement('span');
        position.innerText = 'Position:';

        selectPosition.append(worksOption, worksOptionHead, worksOptionBartender, worksOptiOnchef, worksOptionLoader, worksOptionSorter, worksOptionWaiter, worksOptionCook);
        position.append(selectPosition);

        const inputSalary: HTMLInputElement = document.createElement('input');
        inputSalary.type = 'number';
        inputSalary.name = 'salary';
        if(this.trueAdnFalse) {
            inputSalary.placeholder = String(item!.salary);
        }
        const salary: HTMLSpanElement = document.createElement('span');
        salary.innerText = 'Salary:';
        salary.append(inputSalary);

        const worksSelect: HTMLSelectElement = document.createElement('select');
        worksSelect.name = 'works';

        const worksOptionСhoice: HTMLOptionElement = document.createElement('option');
        if(this.trueAdnFalse) {
            worksOptionСhoice.innerText = String(item!.works);
        }
        const worksOptionTrue: HTMLOptionElement = document.createElement('option');
        worksOptionTrue.innerText = 'true';
        worksOptionTrue.value = 'true';

        const worksOptionFalse: HTMLOptionElement = document.createElement('option');
        worksOptionFalse.innerText = 'false';
        worksOptionFalse.value = 'false';
        
        const works: HTMLSpanElement = document.createElement('span');
        works.innerText = 'Works:';
        
        worksSelect.append(worksOptionСhoice, worksOptionTrue, worksOptionFalse);
        works.append(worksSelect);

        const selectDepartmentName: HTMLSelectElement = document.createElement('select');
        selectDepartmentName.name = 'departmentName';

        const optionDepartmentName: HTMLOptionElement = document.createElement('option');
        if(this.trueAdnFalse) {
            optionDepartmentName.innerText = item!.departmentName;
        }
        const hallOptionDepartmentName: HTMLOptionElement = document.createElement('option');
        hallOptionDepartmentName.innerText = 'hall';
        hallOptionDepartmentName.value = 'hall';

        const worksOptionKitchen: HTMLOptionElement = document.createElement('option');
        worksOptionKitchen.innerText = 'kitchen';
        worksOptionKitchen.value = 'kitchen';

        const worksOptionWarehouse: HTMLOptionElement = document.createElement('option');
        worksOptionWarehouse.innerText = 'warehouse';
        worksOptionWarehouse.value = 'warehouse';

        const departmentName: HTMLSpanElement = document.createElement('span');
        departmentName.innerText = 'DepartmentName:';
        
        selectDepartmentName.append(optionDepartmentName, hallOptionDepartmentName, worksOptionKitchen, worksOptionWarehouse);
        departmentName.append(selectDepartmentName);

        const button: HTMLButtonElement = document.createElement('button');
        button.innerText = 'Add';
        button.addEventListener('click', this.chengeUserAndAddUser.bind(this, index));

        modalChange.append(modalChangeCap, name, surname, position, salary, works, departmentName, button);

        this.modal!.append(modalChange);
        if(this.modal !== null){
            this.modal.addEventListener('click', this.closeModal.bind(this, this.modal));
        }
    }

    onChengeUser(event: any, index: number){
        event.preventDefault();

        let data: FormData = new FormData(event.target.closest('form'));

        employeeObjectss[index].name = String(data.get('name')) || employeeObjectss[index].name;
        employeeObjectss[index].surname = String(data.get('surname')) || employeeObjectss[index].surname;
        employeeObjectss[index].position = String(data.get('position')) || employeeObjectss[index].position;
        employeeObjectss[index].positionNumber = Number(positionNumber[Number(data.get('position'))]) || employeeObjectss[index].positionNumber;
        employeeObjectss[index].salary = Number(data.get('salary')) || employeeObjectss[index].salary;
        employeeObjectss[index].works = this.eventActive[String(data.get('works'))]
        employeeObjectss[index].departmentName = String(data.get('departmentName')) || employeeObjectss[index].departmentName;
        employeeObjectss[index].departmentNumber = Number(departmentNumber[Number(data.get('departmentName'))]) || employeeObjectss[index].departmentNumber;

        this.renderUsers();
        this.modal!.classList.remove('active');
    }

    onAddUser(event: any){
        event.preventDefault();
        let mass: FormDataEntryValue[] = [];
        let data: FormData = new FormData(event.target.closest('form'));

        data.forEach((item) => {
            mass.push(item);
        });

        employeeObjectss.push({
            name: String(data.get('name')),
            surname: String(data.get('surname')),
            position: String(data.get('position')),
            positionNumber: Number(positionNumber[Number(data.get('position'))]),
            salary: Number(data.get('salary')),
            works: this.eventActive[String(data.get('works'))],
            departmentNumber: Number(departmentNumber[Number(data.get('departmentName'))]),
            departmentName: String(data.get('departmentName')),
        });

        this.renderUsers();
        this.modal!.classList.remove('active');
    }

    chengeUserAndAddUser(index: number | undefined, event: MouseEvent){
        if(this.trueAdnFalse){
            this.onChengeUser(event, index!);
        } else {
            this.onAddUser(event);
        }
    }

    chengeUsers(item: TypeEmployee, index: number){
        this.modal!.classList.add('active');
        this.trueAdnFalse = true;
        this.createModalUsers(item, index);
    }

    deleteUsers(index: number){
        employeeObjectss.splice(index, 1);
        this.renderUsers();
    }

    closeModal(param: HTMLElement, event: MouseEvent){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }
}

const renderCards = new RenderCards();

class Header extends RenderCards{
    statistics: Statistics;
    statisticsModal: HTMLElement | null;
    header: HTMLElement | null;
    constructor(){
        super();
        this.statistics = statistics;
        this.statisticsModal = document.querySelector<HTMLElement>('.statisticsModal') as HTMLElement;
        this.header = document.querySelector<HTMLElement>('.header') as HTMLElement;
        this.headerButtons();
    }
    
    headerButtons(){
        const statistics: HTMLButtonElement = document.createElement('button');
        const addUserButton: HTMLButtonElement = document.createElement('button');
        const restaurantLink: HTMLAnchorElement = document.createElement('a');
        const bankLink: HTMLAnchorElement = document.createElement('a');

        statistics.innerText = 'Statistics';
        addUserButton.innerText = 'Add user';
        restaurantLink.innerText = 'Restaurant';
        restaurantLink.href = './restauran.html';
        bankLink.innerText = 'Bank';
        bankLink.href = './bankHtml.html';
        statistics.addEventListener('click', this.activeStatistics.bind(this));
        if(this.statisticsModal !== null){
            this.statisticsModal.addEventListener('click', this.closeModal.bind(this, this.statisticsModal));
        }
        addUserButton.addEventListener('click', this.addUser.bind(this));
        
        return this.header!.append(statistics, addUserButton, restaurantLink, bankLink);
    }

    activeStatistics(){
        this.statisticsModal!.classList.add('active');
        this.statistics.sumSalariesDepartment();
        this.statistics.averageSalaryDepartment();
        this.statistics.salarysDepartment();
        this.statistics.salarysPosition();
        this.statistics.countDismissed();
        this.statistics.departmentsNoManager();
    }

    addUser(event: MouseEvent){
        this.modal!.classList.add('active');
        this.trueAdnFalse = false;
        this.createModalUsers();
        event.stopPropagation();
    }

    closeModal(param: HTMLElement, event: MouseEvent){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }
}

const header = new Header();
